# python3 rnn-annotate.py --paramfile tagger_param --annotatefile sentences.txt

#!/usr/bin/env python3
import argparse
import torch
from Data import Data, sentences

parser = argparse.ArgumentParser()
parser.add_argument('--paramfile', required=True)
parser.add_argument('--annotatefile', required=True)
args = parser.parse_args()

test_data = Data(args.paramfile+'.io')
model = torch.load(args.paramfile+'.rnn', map_location=torch.device('cpu'))
test_sents = sentences(args.annotatefile)

for sent in test_sents:
    output_ids = model(torch.tensor(test_data.words2ids(sent))).argmax(1)
    output_tags = test_data.ids2tags(output_ids.tolist())
    print(' '.join('{0: <20}'.format(word) for word in sent))
    print(' '.join('{0: <20}'.format(tag) for tag in output_tags), '\n')