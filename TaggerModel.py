#!/usr/bin/env python3

import torch
import torch.nn as nn

class TaggerModel(nn.Module):
    def __init__(self, num_words, num_tags, emb_size, rnn_size, hidden_size, dropout_rate):
        super(TaggerModel, self).__init__()
        self.embedding = nn.Embedding(num_words+1, emb_size)
        self.lstm = nn.LSTM(input_size=emb_size, hidden_size=rnn_size, bidirectional=True)
        self.dropout = nn.Dropout(dropout_rate)
        self.fc1 = nn.Linear(in_features=rnn_size*2, out_features=hidden_size)
        self.fc2 = nn.Linear(in_features=hidden_size, out_features=num_tags)

    def forward(self, text):
        embed_out = self.embedding(text)
        embed_out = self.dropout(embed_out)
        lstm_out, (h, c) = self.lstm(torch.unsqueeze(embed_out, 1))
        lstm_out = self.dropout(torch.squeeze(lstm_out, 1))
        fc_out = torch.tanh(self.fc1(lstm_out))
        out_logits = self.fc2(fc_out)
        return out_logits


def run_tests():
    print('Running tests\n')
    test_word_ids = [0, 9, 1, 5, 6]
    print('Test word IDs:', test_word_ids)
    tagger = TaggerModel(num_words=10, num_tags=3+1, emb_size=10, rnn_size=128, hidden_size=64, dropout_rate=0.2)
    print('\nModel created\n')
    print(tagger)
    print('\nTesting forward method\n')
    tag_scores = tagger(torch.LongTensor(test_word_ids))
    print(tag_scores)
    print(tag_scores.argmax(1))

if __name__ == '__main__':
    run_tests()