#!/usr/bin/env python3
import json
from collections import Counter

class Data:
    def __init__(self, *args):
        if len(args) == 1:
            self.init_test(*args)
        else:
            self.init_train(*args)

    def init_test(self, paramfile):
        ''' reads parameters from .io file '''
        with open(paramfile) as params:
            self.word2id, self.id2tag = json.load(params)
            self.id2tag = {int(id):tag for id, tag in self.id2tag.items()} # original keys in json are str

    def init_train(self, trainfile, devfile, num_words):
        '''
        Attributes:
            train_sents: list, elements in the form (['word', 'word'], ['tag', 'tag'])
            dev_sents: comp train_sents
            word2id: dict
            tag2id: dict
            id2tag: dict
            num_tags: int
        '''
        self.train_sents = self.read_data(trainfile)
        self.dev_sents = self.read_data(devfile)
        word2freq = Counter(word for words, _ in self.train_sents for word in words)
        tag2freq = Counter(tag for _, tags in self.train_sents for tag in tags)
        sorted_words = [word for word, freq in sorted(word2freq.items(), key=lambda x:x[1], reverse=True)][:num_words]
        sorted_tags = [tag for tag, freq in sorted(tag2freq.items(), key=lambda x:x[1], reverse=True)]
        self.num_tags = len(sorted_tags)
        self.word2id = {word: index+1 for index, word in enumerate(sorted_words)} # reserve index 0 for unk
        self.tag2id = {tag: index+1 for index, tag in enumerate(sorted_tags)}
        self.id2tag = {id: tag for tag, id in self.tag2id.items()}

    def read_data(self, filename):
        ''' returns a list of tuples, each containing a list with words and a list with tags '''
        temp_sent_words = []
        temp_sent_tags = []
        sentences = [] # contains final tuples
        with open(filename) as file:
            for line in file:
                if len(line.split()) == 2:
                    word, tag = line.split()
                    temp_sent_words.append(word)
                    temp_sent_tags.append(tag)
                else:
                    sentences.append((temp_sent_words, temp_sent_tags))
                    temp_sent_words = []
                    temp_sent_tags = []
        return sentences


    def words2ids(self, words):
        return [self.word2id.get(word, 0) for word in words]

    def tags2ids(self, tags):
        return [self.tag2id.get(tag, 0) for tag in tags]

    def ids2tags(self, ids):
        return [self.id2tag[id] for id in ids]

    def store_parameters(self, paramfile):
        ''' save word2id, id2tag to file '''
        with open(paramfile+'.io', 'w') as dumpfile:
            dumpfile.write(json.dumps([self.word2id, self.id2tag]))


def sentences(filename):
    with open(filename) as file:
        for line in file:
            yield line.split()

def run_tests():
    print('Running tests\n')
    trainfile = 'train.tagged'
    devfile = 'dev.tagged'
    data = Data(trainfile, devfile, 10000)
    print('Testing the format of train_sents\n')
    print(data.train_sents[:3])
    print('\nTesting indices of most frequent words\n')
    print(list(data.word2id.items())[:20])
    print('\nTesting indices of least frequent words\n')
    print(list(data.word2id.items())[-20:])
    print('\nTesting words2ids\n')
    test_words = ['der', 'den', 'des', 'ddd']
    print(' '.join(test_words))
    print(data.words2ids(test_words))
    print('\nTesting tags2ids & ids2tags\n')
    test_tags = ['ADV', 'VAFIN.3.Sg.Pres.Ind', 'NN.Dat.Sg.Masc']
    print(' '.join(test_tags))
    tag_ids = data.tags2ids(test_tags)
    print(tag_ids)
    remapped_tags = data.ids2tags(tag_ids)
    print(remapped_tags)



if __name__ == '__main__':
    run_tests()
