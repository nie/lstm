# python3 rnn-train.py --trainfile train.tagged --devfile dev.tagged --paramfile tagger_param --num_epochs=20 --num_words=10000 --emb_size=100 --rnn_size=256 --hidden_size=256 --dropout_rate=0.4 --learning_rate=0.001

#!/usr/bin/env python3
from tqdm import tqdm # remove for submission

import argparse
import random
import torch
import torch.nn as nn
import torch.optim as optim
from Data import Data
from TaggerModel import TaggerModel

parser = argparse.ArgumentParser()
parser.add_argument('--trainfile', required=True)
parser.add_argument('--devfile', required=True)
parser.add_argument('--paramfile', required=True)
parser.add_argument('--num_epochs', type=int, required=True)
parser.add_argument('--num_words', type=int, required=True)
parser.add_argument('--emb_size', type=int, required=True)
parser.add_argument('--rnn_size', type=int, required=True)
parser.add_argument('--hidden_size', type=int, required=True)
parser.add_argument('--dropout_rate', type=float, required=True)
parser.add_argument('--learning_rate', type=float, required=True)
args = parser.parse_args()

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
print('Using {} device'.format(device))

train_data = Data(args.trainfile, args.devfile, args.num_words)
train_data.store_parameters(args.paramfile) # to be used by rnn-annotate.py
model = TaggerModel(args.num_words, train_data.num_tags+1, args.emb_size, args.rnn_size, args.hidden_size, args.dropout_rate)
model.to(device)
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=args.learning_rate)

best_eval_acc = 0
for e in range(args.num_epochs):
    random.shuffle(train_data.train_sents)
    model.train()
    epoch_total_train_acc = 0
    for words, tags in tqdm(train_data.train_sents, unit='sent'):
        words = torch.tensor(train_data.words2ids(words))
        tags = torch.tensor(train_data.tags2ids(tags))
        words, tags = words.to(device), tags.to(device)
        optimizer.zero_grad()
        output = model(words) # logits shape (sent_len, num_tags)
        sent_loss = criterion(output, tags)
        sent_acc = int(sum([output.argmax(1)[i] == tags[i] for i in range(len(tags))]))/len(tags)
        epoch_total_train_acc += sent_acc
        sent_loss.backward()
        optimizer.step()
    epoch_avg_train_acc = round(epoch_total_train_acc/len(train_data.train_sents), 4)

    model.eval()
    epoch_total_eval_acc = 0
    with torch.no_grad():
        for words, tags in tqdm(train_data.dev_sents, unit='sent'):
            words = torch.tensor(train_data.words2ids(words))
            tags = torch.tensor(train_data.tags2ids(tags))
            words, tags = words.to(device), tags.to(device)
            output = model(words)
            sent_acc = int(sum([output.argmax(1)[i] == tags[i] for i in range(len(tags))]))/len(tags)
            epoch_total_eval_acc += sent_acc
    epoch_avg_eval_acc = round(epoch_total_eval_acc/len(train_data.dev_sents), 4)
    if epoch_avg_eval_acc > best_eval_acc:
        best_eval_acc = epoch_avg_eval_acc
        print('* validation accuracy improved, saving model to ' + args.paramfile + '.rnn')
        torch.save(model, args.paramfile+'.rnn')
    print('Epoch', e+1, 'Train Acc:', epoch_avg_train_acc, 'Validation Acc:', epoch_avg_eval_acc)